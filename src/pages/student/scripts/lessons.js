import {lessons , timeSlots} from "./constants";

const formLessons = document.getElementById('formLessons');
const button = formLessons.querySelector('.button');

button.addEventListener('click', (event) => {
    event.preventDefault();

    const typeLessonRadioBtn = formLessons.querySelector('input[name="type"]:checked');
    const timeLessonRadioBtn = formLessons.querySelector('input[name="time"]:checked');

    const typeLessonId = typeLessonRadioBtn.getAttribute('id');
    const typeLesson = lessons[typeLessonId];  // - got type of lesson

    const timeLessonId = timeLessonRadioBtn.getAttribute('id');
    const timeLesson = timeSlots[timeLessonId];  // - booked time
    let tomorrow = true;

    if (timeLessonId === 'time_01' || timeLessonId === 'time_02' || timeLessonId === 'time_03') {
        tomorrow = false;
    }

    const userObject = JSON.parse(localStorage.getItem('login'));  // - got user object
    const addLessons = JSON.parse(localStorage.getItem('lessons')) || [];

    const objectLesson = {
        name: userObject.name,
        time: timeLesson,
        tomorrow: tomorrow,
        title: typeLesson.title,
        duration: typeLesson.duration
    };

    localStorage.setItem('lessons', JSON.stringify([objectLesson, ...addLessons]));
});
