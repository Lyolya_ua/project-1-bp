const planedLessons = document.getElementById('planedLessons');

const lessons = JSON.parse(localStorage.getItem('lessons')) || [];

function getLesson(lesson) {
    const {name, time, tomorrow, title, duration} = lesson;
    const isTomorrow = tomorrow ? 'Завтра' : 'Сегодня';
    const fromTime = time + ':00';
    const toTime = (duration === 120) ? (time + 2) + ':00' : (duration === 90) ? (time + 1) + ':30' : (time + 1) + ':00' ;
    const image = randomInteger(2, 3);

    return `<div class="card-box">
			    <div class="card-illustration">
					<img src="./images/user_0${image}.png" alt="">
				</div>
				<div class="info">
					<p class="sub-title">${isTomorrow}, ${fromTime} — ${toTime}</p>
					<p class="info-title">${name}</p>
					<p class="info-desc">${title}</p>
				</div>
			</div>`;
}

function generateHTML () {
    const oldContent = document.querySelectorAll('.block__scheduled-lessons .card-box');

    oldContent.forEach(item => item.remove());

    const lessonsHtml = lessons.map(item => {
        return getLesson(item);
    }).join('');

    planedLessons.insertAdjacentHTML('afterend', lessonsHtml);
}

generateHTML();

function randomInteger(min, max) {
    const rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}
