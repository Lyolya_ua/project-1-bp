import { saveToStorage } from './login';

const user = {};
const btnRegister = document.getElementById('regBtn');
const btnContinue = document.getElementById('toStep2Btn');
const btnCreateAccount = document.getElementById('createAccount');

const loginBlock = document.getElementById('loginBlock');
const step1Block = document.getElementById('step1Block');
const regBlock = document.getElementById('regBlock');

const toLoginSvg = document.getElementById('toLoginSvg');
const from3to2Svg = document.getElementById('from3to2Svg');

const from1to2 = document.getElementById('from1to2');
const from2to1 = document.getElementById('from2to1');

function sendForm () {
    const name = document.getElementById('name');
    const nameValue = name.value.trim();
    const email = document.getElementById('email');
    const emailValue = email.value.trim();
    const isEmail = (getEmail(emailValue) === '@');
    const password = document.getElementById('password');
    const passwordValue = password.value.trim();
    const passwordNext = document.getElementById('password_next');
    const passwordNextValue = passwordNext.value.trim();
    const arrayName = name.value.split(' ');

    function getEmail(email) {
        let array = Array.from(email);
        let filter = array.filter(item => {
            return item === '@';
        });

        return filter.join('');
    }

    const input = step1Block.querySelector('form input:checked');
    const type = input.id === 'user_teacher' ? 'teacher' : 'student';
    user.type = type;

    if(nameValue === '' && !isEmail && passwordValue === '') {
        name.classList.add('error');
        email.classList.add('error');
        password.classList.add('error');
        passwordNext.classList.add('error');
        console.error('Введите валидные данные');
        return ;
    } else if(arrayName.length !== 2) {
        name.classList.add('error');
        console.error('Введите валидное имя и фамилию');
        return ;
    } else if (!isEmail) {
        email.classList.add('error');
        console.error('Введите валидный email');
        return ;
    } else if (passwordValue === '' || passwordValue !== passwordNextValue) {
        password.classList.add('error');
        console.error('Введите валидный пароль');
        return ;
    } else {
        user.name = nameValue;
        user.email = emailValue;
        user.password = passwordValue;

        let errorArray = document.querySelectorAll('.error');
        errorArray.forEach(item => {
            item.classList.remove('error');
        });

        if (user.type === 'teacher') {
            localStorage.setItem('teacher', JSON.stringify(user));
            console.log(localStorage);
        } else {
            const students = JSON.parse(localStorage.getItem('students')) || [];

            localStorage.setItem('students', JSON.stringify([user, ...students]));
            console.log(localStorage);
        }

        saveToStorage(user);

        regBlock.querySelector('form').reset();

        window.location.href = user.type === 'teacher' ? 'teacher.html' : 'student.html';

        return user;
    }
}

regBlock.querySelectorAll('input').forEach(item => {
    item.addEventListener('onchange', () => {
        item.classList.remove('error');
    })
});

btnRegister.addEventListener('click', () => {
    step1Block.style.display = 'flex';
    loginBlock.style.display = 'none';
});

toLoginSvg.addEventListener('click', () => {
    step1Block.style.display = 'none';
    loginBlock.style.display = 'flex';
});

btnContinue.addEventListener('click', () => {
    loginBlock.style.display = 'none';
    step1Block.style.display = 'none';
    regBlock.style.display = 'flex';
});

from3to2Svg.addEventListener('click', () => {
    step1Block.style.display = 'flex';
    regBlock.style.display = 'none';
    loginBlock.style.display = 'none';
});

from2to1.addEventListener('click', () => {
    step1Block.style.display = 'flex';
    regBlock.style.display = 'none';
    loginBlock.style.display = 'none';
});

from1to2.addEventListener('click', () => {
    loginBlock.style.display = 'none';
    step1Block.style.display = 'none';
    regBlock.style.display = 'flex';
});

btnCreateAccount.addEventListener('click', () => {
    sendForm();
    console.log(user);
});
